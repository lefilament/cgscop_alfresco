# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import models, fields, api


class AlfrescoPartnerFiles(models.TransientModel):
    _name = 'alfresco.partner.files'
    _inherit = 'cgscop.alfresco'
    _description = "Requete fichiers par organisme"

    name = fields.Char('Nom du Fichier')
    type = fields.Char('Type')
    periode = fields.Char('Période')
    validite = fields.Char('Validité')
    file_id = fields.Char('ID Fichier')
    last_modification = fields.Datetime('Dernière Modification')
    user_id = fields.Many2one(
        comodel_name='res.users',
        string='User')
    partner_id = fields.Many2one(
        comodel_name='res.partner',
        string='Organisme')

    @api.multi
    def download_file(self):
        for doc in self:
            return {
                'type': 'ir.actions.act_url',
                'url': '/web/binary/download_alfresco?id_alfresco=%s' % (
                    doc.file_id, ),
                'target': 'self',
                }


