# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

import dateutil.parser
import pytz

from odoo import models, fields, api

ALFRESCO_TRACKED_FIELDS = ['name', 'id', 'siret', 'member_number']


class AlfrescoPartner(models.Model):
    _name = 'res.partner'
    _inherit = ['res.partner', 'cgscop.alfresco']

    id_alfresco = fields.Char('ID Dossier Alfresco')

    # ------------------------------------------------------
    # Fonctions héritées
    # ------------------------------------------------------

    @api.multi
    def write(self, vals):
        """ Surcharge la fonction write() pour mettre à jour
        la raison sociale, le nom, le siret, l'id Odoo ou le
        num adhérent
        """
        tracked_fields = self.fields_get(ALFRESCO_TRACKED_FIELDS)
        # Dict des valeurs initiales des champs présents dans TRACKED_FIELDS
        initial_values = dict(
            (record.id, dict(
                (key, getattr(record, key)) for key in tracked_fields)) for record in self.filtered('is_cooperative'))
        # Ecriture des nouvelles valeurs
        result = super(AlfrescoPartner, self).write(vals)
        # Dict des nouvelles valeurs
        new_values = dict(
            (record.id, dict(
                (key, getattr(record, key)) for key in tracked_fields)) for record in self.filtered('is_cooperative'))
        # Check des modifications sur les coopératives présentes dans RIGA
        for record in self:
            if record.is_cooperative and record.id_alfresco:
                if new_values[record.id] != initial_values[record.id]:
                    self.alfresco_update_organism(record)
        return result

    @api.multi
    def unlink(self):
        for partner in self:
            if partner.id_alfresco:
                partner.alfresco_remove(partner.id_alfresco)

    # ------------------------------------------------------
    # Fonction boutons
    # ------------------------------------------------------
    def get_partner_files(self):
        """ Liste l'ensemble des fichiers pour un organisme

        La fonction fait appel à l'API Alfresco et enregistre le résultat
        dans un modèle Transient.
        L'ID user est ajouté à la table pour définir les requêtes propres à
        chaque user

        @return: ir.act.window
        """
        if self.id_alfresco:
            files = self.alfresco_list_docs(self.id_alfresco).get('docs', '')
        else:
            self.id_alfresco = self.alfresco_create_organism(self)
            print(self.name)
            print(self.id_alfresco)
            files = self.alfresco_list_docs(self.id_alfresco).get('docs', '')
        uid = self.env.user.id
        alfresco_obj = self.env["alfresco.partner.files"]
        alfresco_obj.search([['user_id', '=', uid]]).unlink()
        for doc in files:
            alfresco_obj.create({
                'name': doc['name'],
                'type': doc['type'],
                'file_id': doc['nodeRef'].replace('workspace://SpacesStore/', ''),
                'user_id': uid,
                'periode': doc['periode'],
                'validite': doc['validite'],
                'last_modification': dateutil.parser.parse(
                    doc['modifiedOn']).astimezone(pytz.utc),
                'partner_id': self.id,
            })
        return {
            "type": "ir.actions.act_window",
            "name": "Fichiers liés",
            "res_model": "alfresco.partner.files",
            "views": [[False, "tree"]],
            "search_view_id": self.env.ref(
                "cgscop_alfresco.alfresco_partner_files_search").id,
            'context': {'search_default_group_periode': True}
        }

    # ------------------------------------------------------
    # CRON
    # ------------------------------------------------------
    def _init_alfresco_folder(self):
        """ Fonction pour le cron permettant de créer un dossier
        dans Alfresco pour l'ensemble des organismes qui n'ont pas
        de nodeRef
        """
        partners = self.env['res.partner'].search([
            ['is_cooperative', '=', True],
            ['id_alfresco', '=', False],
            ['membership_status', '=', "member"]])
        for partner in partners:
            id_alfresco = partner.alfresco_create_organism(partner)
            partner.write({
                'id_alfresco': id_alfresco,
            })
