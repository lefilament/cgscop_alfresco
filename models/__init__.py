# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from . import api_alfresco
from . import alfresco_connection
from . import alfresco_partner_files
from . import res_partner
