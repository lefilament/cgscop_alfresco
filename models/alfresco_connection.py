# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import models, fields


class CgscopAlfrescoConnection(models.Model):
    _name = 'alfresco.connection'
    _description = 'Infos API de connexion pour Alfresco'

    name = fields.Char(string='Nom')
    login = fields.Char('Login')
    password = fields.Char('Password')
    url = fields.Char('URL')
    ssl = fields.Boolean('Connexion SSL', default=False)
    active = fields.Boolean('Actif', default=False)

    def toogle_active_config(self):
        """ Active la connexion sélectionnée et désactive
            toutes les autres connexions
        """
        for item in self.env['alfresco.connection'].search([]):
            item.active = False
        self.active = True
