# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

import base64

from odoo import http
from odoo.http import request
from odoo.addons.web.controllers.main import serialize_exception, content_disposition


class AlfrescoBinary(http.Controller):
    @http.route('/web/binary/download_alfresco', type='http', auth="user")
    @serialize_exception
    def download_alfresco_document(self, id_alfresco, **kwargs):
        """ Téléchargement des fichiers depuis alfresco.

        :param str id_alfresco: id de la ressource
        @return: :class:`werkzeug.wrappers.Response`
        """
        doc = request.env['alfresco.partner.files'].search(
            [['file_id', '=', id_alfresco]]).alfresco_get_doc(id_alfresco)
        filecontent = base64.b64decode(
            doc['base64NodeContent'])
        if not filecontent:
            return request.not_found()
        else:
            return request.make_response(filecontent, [
                ('Content-Type', 'application/octet-stream'),
                ('Content-Disposition', content_disposition(doc['name']))])
