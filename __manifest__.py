{
    "name": "CG SCOP - Connecteur Alfresco",
    "summary": "Connecteur Alfresco",
    "version": "12.0.1.0.1",
    "development_status": "Beta",
    "author": "Le Filament",
    "license": "AGPL-3",
    "application": False,
    "depends": [
        'base',
        'web',
        'cgscop_partner',
        'web_ir_actions_close_wizard_refresh_view',
    ],
    "data": [
        "security/ir.model.access.csv",
        "views/assets.xml",
        "views/alfresco_connection.xml",
        "views/alfresco_partner_files.xml",
        "views/res_partner.xml",
        "wizard/add_file_wizard.xml",
        "datas/cron_init_alfresco.xml",
    ],
    'qweb': [
        'static/src/xml/*.xml',
    ],
    'installable': True,
    'auto_install': False,
}
