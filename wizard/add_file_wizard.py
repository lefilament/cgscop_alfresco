# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

import mimetypes
import dateutil.parser
import pytz

from odoo import api, fields, models
from odoo.tools.mimetypes import guess_mimetype


class AddFileWizard(models.TransientModel):
    """ Wizard d'ajout de fichier dans la GED

    Récupère automatiquement depuis Alfresco les valeurs de periode,
    type et validité ainsi que l'organisme associé
    """
    _name = 'add.file.wizard'
    _inherit = 'cgscop.alfresco'
    _description = "Ajout de fichier dans Alfresco"

    # Default functions
    def _get_selection(self, value):
        try: 
            file_type = self.alfresco_list_type()[value]
            select_type = []
            for type in file_type:
                select_type.append(
                    (type, type))
            return select_type
        except:
            return False

    @api.model
    def _get_type(self):
        return self._get_selection('type')

    @api.model
    def _get_periode(self):
        return self._get_selection('periode')

    @api.model
    def _get_validite(self):
        return self._get_selection('validite')

    @api.model
    def _default_partner_id(self):
        return self.env.context.get('active_id')

    type = fields.Selection(selection=_get_type, string='Type')
    periode = fields.Selection(selection=_get_periode, string='Période')
    validite = fields.Selection(selection=_get_validite, string='Validité')
    file = fields.Binary('Fichier')
    filename = fields.Char('Nom')
    partner_id = fields.Many2one(
        comodel_name='res.partner',
        string='Organisme',
        default=_default_partner_id,)

    @api.multi
    def add_file(self):
        """ Ajoute un fichier sur la GED Alfresco

        @return: fonction get_partner_files() de res.partner
        """
        # Get Mimetype
        content_type = mimetypes.guess_type(self.filename)
        if content_type[0]:
            content_type = content_type[0]
        else:
            content_type = guess_mimetype(self.file)
        # Upload File
        uploaded_file = self.alfresco_upload(
            partner=self.partner_id,
            type=self.type,
            periode=self.periode,
            validite=self.validite,
            filename=self.filename,
            mimetype=content_type,
            doc=self.file.decode('utf-8'))
        # Add new file in transient model for view
        uid = self.env.user.id
        alfresco_obj = self.env["alfresco.partner.files"]
        alfresco_obj.create({
            'name': self.periode if self.periode != 'Autre' else self.filename,
            'type': self.type,
            'file_id': uploaded_file,
            'user_id': uid,
            'periode': self.periode,
            'validite': self.validite,
            'last_modification': fields.Datetime.now(),
            'partner_id': self.partner_id.id,
        })
        # Reload view
        return {
            'type': 'ir.actions.close_wizard_refresh_view'
        }
