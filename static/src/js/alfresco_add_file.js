odoo.define('connector_alfresco.alfresco_add_file', function (require){
"use strict";

var core = require('web.core');
var ListController = require('web.ListController');
var QWeb = core.qweb;
var session = require('web.session');

ListController.include({       

    renderButtons: function() {
        this._super.apply(this, arguments);
        var self = this;
        this.$buttons.on('click', '.add_file', function() {
            var active_id = self.getParent().getParent()._current_state.active_id;
            self.do_action({
                type: "ir.actions.act_window",
                name: "Ajouter un fichier",
                res_model: "add.file.wizard",
                views: [[false,'form']],
                target: 'new',
                view_type : 'form',
                view_mode : 'form',
                context: { 'active_id': Number(active_id) }
            });
        });
    },

});

});