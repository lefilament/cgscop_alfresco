.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl
   :alt: License: AGPL-3


=============================
CG SCOP - Connecteur Alfresco
=============================

Description
===========

Ce module permet de lire, créer, supprimer les documents depuis Alfresco.

Il utilise l'API Alfresco développée spécifiquement pour le projet de la CG Scop qui impémente les fonctions suivantes : 

* Création d'un dossier : lors de la création d'un organisme
* Mise à jour d'un dossier : lors des la mise à jour d'un organisme sur les champs nom, SIRET et n° adhérent
* Liste des fichiers pour un organisme
* Ajout d'un fichier
* Suppression d'un dossier : lors de la suppression d'un organisme


Usage
=====

Pour configurer ce module, les données suivantes sont à renseigner dans le menu **Configuration > APIs CG Scop > Alfresco > Configuration connexion** : 

* *alfresco_login* : login de connexion à l'API
* *alfresco_password* : password de connexion à l'API
* *alfresco_url* : url de l'API
* *alfresco_ssl* : **True** si on active la vérification SSL, **False** sinon (False par défaut)
* *active* : si il s'agit de la connexion active


Credits
=======

Funders
------------

The development of this module has been financially supported by:

    Confédération Générale des SCOP (https://www.les-scop.coop)


Contributors
------------

* Juliana Poudou <juliana@le-filament.com>
* Rémi Cazenave <remi@le-filament.com>
* Benjamin Rivier <benjamin@le-filament.com>


Maintainer
----------

.. image:: https://le-filament.com/images/logo-lefilament.png
   :alt: Le Filament
   :target: https://le-filament.com

This module is maintained by Le Filament
