# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

import logging
import base64

from odoo.tests import tagged, common

_logger = logging.getLogger(__name__)


@tagged('post_install', '-at_install')
class TestApiAlfresco(common.TransactionCase):

    def test_read(self):
        print("---------- tests alfresco ----------------")
        alfresco = self.env['cgscop.alfresco']
        
        print("_________ Liste docs Filament _______")
        print(alfresco.alfresco_list_docs('LeFilament'))
        
        print("_________ Liste Type _______")
        list_type = alfresco.alfresco_list_type()
        print(list_type)

        print("_________ Ajoute un dossier _______")
        partner = self.env['res.partner'].browse(11)
        folder = alfresco.alfresco_create_organism(partner)
        print(folder)
        
        print("_________ Ajoute un fichier _______")
        doc_id = self.env['ir.attachment'].browse(695)
        print(doc_id)
        doc = alfresco.alfresco_upload(
            doc_id.datas.decode('utf-8')
        )
        print(doc)

        print("_________ Get fichier _______")
        print(alfresco.alfresco_get_doc(doc))

        print("_________ Suppression d'un fichier _______")
        print(alfresco.alfresco_remove(doc))

        print("_________ Suppression d'un dossier _______")
        print(alfresco.alfresco_remove(folder))

